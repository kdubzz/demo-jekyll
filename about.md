---
layout: page
title: About Me
permalink: /about/
---

Hi!  **I'm Kang**!

I'm a **full-stack developer** and **QA-engineer**.
After grad-school at UBC, I was fortunate to be offered work almost immediately,
first as a _lab assistant_, and then full time as a _data-analyst_.
This was such an exciting time for me at the time, and I was so fortunate 
to be given this opportunity - to work in the same field as my studies
(_biological ocenaography_) - so quickly upon receiving my M.Sc.
If you're curious as to _"what the heck was that guy working on???"_, you can 
find my thesis **[at this link](https://open.library.ubc.ca/cIRcle/collections/ubctheses/24/items/1.0167397)**
or the **[publication, here](https://watermark.silverchair.com/fsx014.pdf?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAApgwggKUBgkqhkiG9w0BBwagggKFMIICgQIBADCCAnoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMAlnvehA_0kAaWWRLAgEQgIICSyVkGytcUqg8MBn-xSMX3MP0SY5OBhN2PrJisSBG0WcpFl2xMZCy1bAL6pCbfXzka2H7RLbQ6gUoIRpnHfofrM4a2xCLPSMw8BxsqSSlSnqccLB7rD0D5zPXatBkcT3D15tUukGVNP97EHOpVzDiOITiKekYserq0yf-rpDXU77mFxxeB64-09qHwkEXFIIj_utuSC4d2snz_TM1tyuNYM0isNSLRIB5ufiTmOgK6a79kTGNjiLt8X9NENErkBhs3zXRVhMptT-_STIC2qqwP8T0xzrX2Ikonb-0ufU1WpBUjD3zJAos2Hnca8RH6DNZiwyNwyPf31lH7Ahs4CpvBWVd9RTzNuNkaVHP0a4O4BuHf0rYu2d4BpQSu8zCv5uUpHuXyjpG4ncrmRKLrv2GPgMGjvABq5Jh3XUe5onWn_NUftNNv11secjfSNzXMoD5A-5LYPuMf6bB0nfqbYzBh7UldZeQ_E0Ga0Zkoeo6GCWAA4rNLqxDxS6CFs3rieZBZq7jITeZqMJNKyIG9yEBKHOwARA9EWGq55GG2k_6t1qbz1q_MXYgS-mEz55H9apwrhdbPsL48z0kh3S4-ocOWc3UQaBaWQRpHxvyGULQbpbGrWnF_JHzRvy7b0iXBFHrQmp2XWH4juh83T9fM994XwKj9mZu64gAssx19ftBKW9WLhJd7pnD7Gh2sZL6_hcfZfX7kEnuqMlB5wAguWHbxhFRyzNuPANfbX8KEW1c1HoT4nhIfkDqhP63kv635sN4DDjdx5QpoPsgWE6Z)**.
But alas, as it turns out, this was not to be and in late 2017, I left the 
world of academia with ambitious goes of entering the world of technology 
to build **_all the things_**!

This was to be one of the hardest and most enduring ventures I've partaken in.
How fitting for a self-proclaimed distance runner.  A real test of endurance.
Grad-school was equally, if not more difficult, but I guess the added bit of
pressure that _my life is (in various ways) potentially dependent on that next
interview_ made the weight of job-hunting feel incredibly frustrating, and very
difficult.
<br/>
Or, perhaps it was that grad-school was (_is_) seen as a _prepatory_ step to take
_before_ entering the _real_ world that makes the rigours of the graduate experience
seem less intense. Who knows...,  I'm finished and now I'm moving on!

Long story short, after one short-term data-visualization contract gig
(inspired by **[this](https://www.youtube.com/watch?v=mkJ-Uy5dt5g)** visualization)
and also a short time gig with a friends company, I found full-time work in late
2018, and I've been there ever since
