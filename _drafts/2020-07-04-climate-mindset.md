---
layout: post
title: "Climate Mindset"
date: 2020-07-04 
categories: "climate change jekyll"
---
I listen to podcasts.  A lot of podcasts.
I'm also concerned about our natural world.
Raised 
by my scientist father and having been trained in biological
oceanography at school, I can't help but think about our
impacts on the biosphere, my home, your home... everyone's home.

**T.E.D. Radio Hour** is one of the great podcasts I listen to, and
back in late May, I happened on **[this episode](https://www.npr.org/2020/05/21/860307097/climate-mindset)**.

At the start of fhe episode, there is mention that we (as a species) _can no
longer afford the luxury of feeling powerless_, and  that as the world was overtaken 
by the COVID-19 pandemic, _half of the global community took 
dedicated and sustained action to protect the most vulnerable_.
<br/>
It's amazing what people are capable of when faced with such adversity - adversity
that threatens our health, our economies, our way of life.  There's another 
big challenge society faces.  One that has been known of for numerous decades, 
and it's called climate change.  Well, more appropriately, _anthropogenically
influenced_ climate change.

Anthropogenically influenced climate change is one of the greatest challenges
society has and will face.  Given the impact of climate change on
[human health](https://www.nature.com/articles/nature04188),
[food supply](https://www.nature.com/articles/367133a0.pdf?origin=ppub)
([here, too](https://aura.abdn.ac.uk/bitstream/handle/2164/3196/S0029665112002832a.pdf?sequence=1)),
[trade unions](https://www.sciencedirect.com/science/article/pii/S0959378011001154?casa_token=AzcbegnTvB8AAAAA:Z3wU2jpkw2WdPd3YoRaogZtC8a0CYzFr_4BfvbW6yCLXS5Og8MyMbkDv8q0ew8-bwB-NWq_Ynw)
([here, too](https://www.sciencedirect.com/science/article/pii/S0959378011001154?casa_token=KplncYUJjoIAAAAA:nVh60FnSttL6zGaRiIyvwV7Nsx6sv41Gm51tPmDq0MEGbukSIY5x9eirwwLn0DkA80yE5lOhPw)),
[biodiversity](https://onlinelibrary.wiley.com/doi/pdf/10.1111/j.1461-0248.2011.01736.x),
and many others, it's a bit puzzling why the international community has not yet
taken drastic measures to mitigate the effects, especially given the forboding 
consequences if we continue with _business as usual_.

This isn't a post written to demean the seriousness of COVID-19, or to say that
COVID-19 isn't as serious of a problem as climate change, this post is simply 
one of curiousity.

Anyways, back to the podcast.

The talk starts with speaker **[Tom Rivertt Carnac](https://www.ted.com/speakers/tom_rivett_carnac)**
highlighting the utmost seriousness of the global climate problem.
In short, the _climate crises_ will be _orders of magnitude worse than the 
COVID-19 pandemic_. What does this even mean anyways?  "Orders of magnitude"?
<br/>
By definition, an _order of magnitude (noun)_ is a class in a system of
classification determined by size, with each class being a number of times 
(usually ten) greater or smaller than the one before. From what I've learned
of statistics at school, when the difference between a trial experiement to
to the control differs by an order of magnitude, it's safe to say that there is
a _significant_ difference without conducting a statistical test (don't quote
me on this).
Succinctly, 10<sup>4</sup> and 10<sup>5</sup> differ by **an order** of magnitude, 
and 10<sup>2</sup> and 10<sup>30</sup> differ by **orders** of magnitude.
<br/>
So, if Climate change will be _orders of magnitude_ (note "orders" - plural)
worse than the COVID-19 pandemic - which is already "worse AF!", 
then the climate crises will be significantly "worse AF"!
Hopefully, **[this YouTube link](https://www.youtube.com/watch?v=FglEbPXxka4)**
will give you a sense of the magnitude (see what I did there) of difference
in an _order of magnitude_.

**[Christina Figueres](https://www.ted.com/talks/christiana_figueres_the_inside_story_of_the_paris_climate_agreement)**
is the second speaker and speaks of **stubborn optimism**.
<br/>
Like many trials faced by society throughout history, society has had to embrace optimism (in some way or form) to overcome
these hurdles, states Christina.
But, with the ever-increasing scientific evidence all pointing to a doom and gloom path, how am I
(or anyone for that matter) able to embrace optimism?

Indeed, the anthropogenic influence on our biosphere is unprecedented, and with
the increasing emissions of greenhouse gases, the [reduced sea-ice cover in the arctic](https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1029/2007GL029703%4010.1002/%28ISSN%291944-8007.GRL40), 
as well as the [impacts on phenology](https://www.pnas.org/content/114/50/13212),
it certainly does feel like we're on a path to some sort of _doom & gloom_ as we "progress" into the future.
<br/>
Now, there's a word - "progress" - I wonder about at times.  I feel like society is always 
_progressing_ (technology-wise typically) in order to solve our current day issues,
though at some point in the future, the technological advancements we've _progressed_
to in the past has now, all of a sudden, become a (potentially) pressing matter
in the now.  I have _no evidence_ to support this, but it really feels like that.
_Doesn't it_?  Did all that even make sense?

I digress...
<br/>
Let's take a look at some of this climate data for ourselves and see what it says.
With all the _free & open-source_ data-repositories out there on the web
([Our World In Data](https://ourworldindata.org/), [Ocean Color](https://oceancolor.gsfc.nasa.gov),
[NOAA - National Ocean & Atmospheric Administration](https://www.nodc.noaa.gov/access/index.html),
[ARGO data](http://www.argo.ucsd.edu/), [MODIS Satellite Data](https://modis.gsfc.nasa.gov/data/)), 
it's become incredibly easy for people to access empirical observational data
(in it's rawest form) at various points in time to tell the story of Earth's response
in the wake of our progress.
Processing the data and making sense of it, though, _could_ be another matter entirely.

For simplicity's sake, I'm going to use _Our World In Data_ as a data source, and yes,
although you can go to the website and look around at the various graphs for the
differing data metrics, this is a chance for me to flex my Python muscle, a 
language which I haven't really been using since grad-school.
I've downloaded the [temperature increase data](https://ourworldindata.org/co2-and-other-greenhouse-gas-emissions)
to play with.

{% highlight python %}

import pandas as pd
import matplotlib.pyplot as plt

fname = './temperature-anomaly.csv'
data = pd.read_csv(fname, sep = ',')

all = data[data['Entity'] == 'Global']
nhem = data[data['Entity'] == 'Northern Hemisphere']
shem = data[data['Entity'] == 'Southern Hemisphere']
trop = data[data['Entity'] == 'Tropics']

fig, axs = plt.subplots(2, 2, sharey = True, sharex = True)
fig.set_figwidth(16)
fig.set_figheight(12)

axs[0, 0].plot(all['Year'], all['Median (℃)'], '--', color = "black", lw = 3, alpha = 0.23, label = 'Median')
axs[0, 0].plot(all['Year'], all['Upper (℃)'], 'o', color = "red", ms = 12, alpha = 0.23, label = 'Upper')
axs[0, 0].plot(all['Year'], all['Lower (℃)'], 'o', color = "blue", ms = 12, alpha = 0.23, label = 'Lower')

axs[0, 1].plot(nhem['Year'], nhem['Median (℃)'], '--', color = "black", lw = 3, alpha = 0.23)
axs[0, 1].plot(nhem['Year'], nhem['Upper (℃)'], 'o', color = "red", ms = 12, alpha = 0.23)
axs[0, 1].plot(nhem['Year'], nhem['Lower (℃)'], 'o', color = "blue", ms = 12, alpha = 0.23)

axs[1, 0].plot(shem['Year'], shem['Median (℃)'], '--', color = "black", lw = 3, alpha = 0.23)
axs[1, 0].plot(shem['Year'], shem['Upper (℃)'], 'o', color = "red", ms = 12, alpha = 0.23)
axs[1, 0].plot(shem['Year'], shem['Lower (℃)'], 'o', color = "blue", ms = 12, alpha = 0.23)

axs[1, 1].plot(trop['Year'], trop['Median (℃)'], '--', color = "black", lw = 3, alpha = 0.23)
axs[1, 1].plot(trop['Year'], trop['Upper (℃)'], 'o', color = "red", ms = 12, alpha = 0.23)
axs[1, 1].plot(trop['Year'], trop['Lower (℃)'], 'o', color = "blue", ms = 12, alpha = 0.23)

axs[0, 0].set_title('Global Temperature Anomaly', fontsize = 19)
axs[0, 1].set_title('N. Hemisphere Temperature Anomaly', fontsize = 19)
axs[1, 0].set_title('S. Hemisphere Temperature Anomaly', fontsize = 19)
axs[1, 1].set_title('Tropics Temperature Anomaly', fontsize = 19)

axs[0, 0].set_ylabel('Temperature (℃)', fontsize = 16)
axs[1, 0].set_ylabel('Temperature (℃)', fontsize = 16)

axs[0, 0].legend(loc = 2, prop = {'size': 16})

for each_ax_row in axs:
    for each_ax in each_ax_row:
        for each_ylabel in each_ax.get_yticklabels():
            each_ylabel.set_fontsize(15)
        for each_xlabel in each_ax.get_xticklabels():
            each_xlabel.set_fontsize(15)

{%  endhighlight %}

<img src="{{ site.baseufl }}/assets/temperature_increase.jpg">

Still a bit rusty, but not too bad...

Anyways, it's pretty clear that temperatures - especially in the Northern 
Hemisphere - have been on a rapid upswing since the 70's, and despite warnings
from scientific communities around the world - since the 1980's, and 
various international agreements enacted to curb the anthropogenic effect
of greenhouse gas emissions (1992 Rio Summit, 1997 Kyoto Protocol, and the
2015 Paris), and repeated warnings from [climate scientists](https://academic.oup.com/bioscience/article/67/12/1026/4605229),
we're still not _seemingly_ making much progress.  At least, not when
it comes to national/international news.
Soooo, what the heck? Kinda feels like there's nothing we can do, right? _RIGHT_?
But as Christina says, we have to be stubborn and hope.  To hope that there _is_ hope!

This leads to the next speaker on the podcast
(oh yeah, I was talking about a podcast episode, right).
Dubbed the _Greta Thunberg of America_,  **[Xiye Bastida](https://www.pbs.org/wnet/peril-and-promise/2019/09/meet-xiye-bastida-americas-greta-thunberg/)**
, a 17 year old (at the time of recording) _climate justice activist_, reiterates the 
importance of staying optimistic in the face of overwhelming adversity.
<br/>
People who know me know that I have a habbit of stabbing myself in the foot at times.
Essentially, there are times - and some times many times - when I inately make a 
problem _more_ complicated such that the proposed solution becomes unnecessarily complicated.
I bring up this point because I feel that as I've grown up, I tend to focus on
the problem in it's entirety, and as such, any potential solution
is so overwhelmingly involved and _"hard"_, it becomes difficult for me to get started on that
solution. Remember when we were all young _whipper-snappers_ and any time we were told of a problem,
a general response would come in the form of, _"Why don't you just fix that..."_.
_"Well, what about this problem, which requires this..., and that..."_ could be 
the response from say, _our parents, perhaps_.  _"Then, why don't you just fix **those**
problems then?"_, we would rebuttle. Things were so much simpler back in those days
, or perhaps, we just had a simpler state of mind back then as youngsters.
Now, all grown up, issues such as climate change is _such_ a huge issue that 
it's hard to know _where_, and _how_ to get started.

What if, instead of focusing on the enormity of the problem at hand, we broke down
the problem into much smaller steps and got started with a solution?
Of course, I'm greatly simplifying the problem, but this mode of thinking allows
you to proceed with a solution, would it not?  Then, we can iterate that solution to acccount
for the complexities of associated problems.  Maybe this is still too simplifyed...,
but the key take-away from Bastida's talk is that we need to start taking action.
Yes, the problems have been identified, as has the consequences of thse problems 
if no action is to be taken, so let's do something. 
[Imagine the future](https://www.npr.org/2020/05/22/860168455/xiye-bastida-how-are-young-people-making-the-choice-to-fight-climate-change)
if we tackled these issues (amongst numerous others) as an international community.

The last speaker is **[Oliver Jeffers](https://www.npr.org/2020/05/22/860144962/oliver-jeffers-an-ode-to-living-on-earth)**,
and I'm not going to say anything about this aside from **_you should have a listen and give
it a bit of thought_**.
