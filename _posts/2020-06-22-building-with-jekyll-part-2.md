---
title: "Building with Jekyll - Part 2"
categories: jekyll
permalink: /posts/:title
---

Continuing on from **[Part 1](https://jibberjabber.netlify.app/posts/building-with-jekyll)**...
<br />
<sub><sup>(So, I actually haven't learnt how to make an _href_ to a previous post when working in 
development mode, yet, so I'm linking it to the actualy live post...yeahhhh)</sup></sub>

Having been recently exposed to _Hugo_, I was pleased to find that the file structure of a 
new Jekyll project is not too dissimilar from that of a Hugo project.

Both projects have a _"config"_ type file (`_config.yml` in Jekyll & `config.toml` in Hugo), 
and from the limited knowledge I have, both types of _config_ files have very similar, if not
the same type of functionality - to _configure_ your project (_betcha didn't see that one 
coming!_). The biggest difference is that for _Hugo_, the defualt `config` file is of `.toml`
format.  I don't have any knowledge about this, so I won't talk about it. _BUT_, 
the config file for a Hugo project can be _configured_ (see what I did there) to different 
formats.  So, if you like `.json`, then you can write your Hugo config as `JSON`, but
if you're more a DevOps kinda person, then `.yml` (`.yaml`) it is!

**So, what have I learned so far** in building this static website so far?
Well, after watching some [tutorial videos](https://www.youtube.com/watch?v=T1itpPvFWHI&list=PLLAZ4kZ9dFpOPV5C5Ay0pHaa0RJFhcmcB)
and checking out the [Jekyll Docs](https://jekyllrb.com/docs/) (which are quite good, 
I find), here are some of the things I've learnt,

---
<br />

- **Front Matter**
  - By definition, _front matter_ is a file's metadata (`yml` format).
  - Meta data such as a post's _title_, _date_, _categories_, _etc._ are specified in the
  files

_Front Matter_ is written like this,

{% highlight yaml %}
---
title: <title>
date: <date>
layout: <layout>
...
<many-more-that-i-have-not-learnt-im-sure>
---
{% endhighlight %}

Now, `layout` is a bit interesting!<br/>
In Jekyll, there are two types of layouts that I've learnt of so far - `post` and `page`.

A `post` layout (with _this_ theme - `minima`) will look like the current page layout that
you're seeing now - nothing too flashy, pretty basic, **but**, this layout does display
some of that good'ol _front matter_ on the page (post _date_ and _author_ - that's me!).

A `page` layout (again, with this theme) is used for..., well, _pages_.  Click on _About_ 
on the main navbar at the top, and compare that page layout with what you see now.

---
<br />

- **File Naming Conventions**
  - One thing I miss about Hugo is the CLI interface...
    - With this, I could simply type in `hugo new <new-post-title>`, and **kabam!**, the new
    post was generated.
    - As far as I know with Jekyll, there isn't such a feature, and new files have to be 
    created manually with the following format - `YYYY-MM-DD-your-new-post-title.md`.
    - The dahes (`-`) are essentially spaces, and with this given format, the new post
    will have an implicit _front matter_ such that the `YYYY-MM-DD` acts as the `date` key,
    and `your-new-post-title` will be the `title` key in your file's _front matter_.

Now, Jekyll will render a post on a URL based on the file naming convention of your post.
So, if a post's file was named `YYYY-MM-DD-some-title.md`, then that post will be rendered
on `http://localhost:4000/YYYY/MM/DD/some-title`, which is absolutely fine..., until you 
start messin' around with that file's _front matter_!

Welcome to `permalinks`!

Also part of your _front matter_ declaration, this could save you from lots of hassle
as the `permalink` defines where (url) your post will be located.
So, if I have a front matter with a `permalink` like,

{% highlight yaml %}
---
...
title: <some-title>
permalink: /posts/:title
...
---
{% endhighlight %}

...then, that post will be served on `http://localhost:4000/posts/<some-title>`.
**Boom**! Problem solved - consistency at last!

---
<br />

More learning to come in the coming days, so stay tuned!
