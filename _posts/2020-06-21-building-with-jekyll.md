---
title: "Building with Jekyll - Part 1"
categories: jekyll
permalink: /posts/:title
---

Last weekend, I started experimenting with **Hugo** to try my
hand at building static websites.

Building a static website is something I've wanted to do for a long time, now, ever since the start of grad-school,
back around 2010-2011.
I had learned of GitHub Pages back then, and fiddled around with the setup... but
long story short, the final product came about through me copying some free bootstrap templates and their 
css-styling files, and pasting them into my project root directory.
Although I didn't have too much knowledge of what I was doing, I have to admit, the GitHub Page **_did_** look pretty good (thanks, Bootstrap!).
Though, after some time, rendering a GH-Page through a basic html that was talking to a css file didn't seem like the most
elegant way to go.
Learning of **Jekyll** and **static web-sites** soon after, I realized I needed to replace my 
`index.html` and `.css` powered GH-Page with a _static web-site_.
**[This link](https://www.youtube.com/watch?v=2MsN8gpT6jY&feature=emb_logo)** summarizes my initial experience with GitHub Pages
pretty well, I think (I put through in some HTML and CSS files into a repo and expected some site dynamicism - I should've been using
a more elegant tool, like Jekyll).

So, last week, after many many years, I started to build one, but with **Hugo**! 

_"Why is this guy talking about Hugo when the post is about Jekyll?"_, you may be thinking.
Simply put, I wanted to try out some of the new tools available, and after hearing about **Go** (or _Golang_)
after completing the _Web-Application Development_ bootcamp at Lighthouse Labs, and hearing how much hype there was
around _Go_, it seemed like a great opportunity to dive in.

Developing on `localhost` was great and I soon learned about the Hugo file-hierchy, and how 
the different files _talked_ to one another. Yet, this was short lived, and after encountering
many deploy issues (first to GitLab Pages - could _not_ get the `.gitlab-ci.yml` file 
set up properly, then to Netlify), it seemed like the css-styling for the selected theme was not being applied.
**In short, I was really bummed**, though I did learn some new things during the process
(`git submodules` - a repo living _within_ a wrapping repo essentially, and **[Netlify](https://www.netlify.com/)** - thanks Botch).

...and at long last, here I am, **buidling with Jekyll**!
