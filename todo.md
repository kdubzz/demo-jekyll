---
title: Project To-Do's
layout: page
permalink: /todos/
---

This is a place where I have _allllll_ the things I still have left to do, 
and to learn in the process, while I build out this static website.

| To Do | Status | Completed - for now |
|-------|--------|---------------------|
| Drink Coffee | &#x2714; | **_On-Going!_** |
| Make a page to list out all the things I have to do to build out this static website | &#x2714; | Jun 27, 2020 |
| Learn how to [make a table in markdown](https://idratherbewriting.com/documentation-theme-jekyll/mydoc_tables.html) | &#x2714; | Jun 27, 2020 |
| Add content to the [About page](https://jibberjabber.netlify.app/about/) | &#x2714; | June 30, 2020 |
| Add content to the [Contact page](https://jibberjabber.netlify.app/contact/) | &#x2718; |
| Figure out how to add a block to the home page where I can show an image and write a short block of text (may require some CSS magic) | &#x2718; |
| Add various social links to the _footer_ of the current Jekyll theme | &#x2718; |
| Experiment with _other Jekyll themes_ | &#x2718; |
| Add Strava-link to the header bar | &#x2718; |
